import Provider from 'torii/providers/oauth2-bearer';
import {configurable} from 'torii/configuration';

/**
 * This class implements authentication against Azure
 * using the OAuth2 authorization flow in a popup window.
 * @class
 */
var GitlabOauth2 = Provider.extend({
  name:       'gitlab-oauth2',
  baseUrl:    'https://gitlab.com/oauth/authorize',

  // additional url params that this provider requires
  requiredUrlParams: ['client_id'],
  responseParams: ['code'],

  clientId: '4c605021ea85362d64ef8511b8b509d41df49ea98b59421a69f922fe44dd2e92',

  redirectUri: 'http://localhost:4200',
});

export default GitlabOauth2;
